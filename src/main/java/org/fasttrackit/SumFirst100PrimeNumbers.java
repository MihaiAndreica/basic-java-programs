package org.fasttrackit;

public class SumFirst100PrimeNumbers {
    public static void main(String[] args) {
        int sum = 2;
        int count = 1;
        for (int i = 3; count < 100; i += 2) {
            if (isPrime(i)) {
                sum += i;
                count++;
            }
        }
        System.out.println("Sum of the first 100 prime numbers: " + sum);
    }
    public static boolean isPrime(int number) {
        for (int i = 2; i < number / 2; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return number > 1;
    }
}