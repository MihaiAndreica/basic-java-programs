package org.fasttrackit;

import java.util.Scanner;

public class FebruaryCount {
    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        int year;

        System.out.println("For which year, between 1900 and 2016, do you want to find out how many days February had?");
        year = console.nextInt();
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
            System.out.println("Number of days in February " + year + " was 29 days, so it was a leap year!");
        } else {
            System.out.println("Number of days in February " + year + " was 28 days!");
        }

    }
}
// Fiecare an care e divizibil cu 4 (adică se împarte la 4 fără rest) e an bisect;
// dar dacă e divizibil și cu 100 nu e an bisect decât dacă e divizibil și cu 400.
// 2000 e diviz la 4 (deci e bisect), dar e diviz si la 100 (deci nu e bisect), dar se divide si la 400, Deci E AN Bisect