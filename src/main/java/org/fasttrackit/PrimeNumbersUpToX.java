package org.fasttrackit;

import java.util.Scanner;

public class PrimeNumbersUpToX {
    public static void main(String[] args) {
        int x;
        int i = 0;
        int num = 0;
        Scanner scanner =new Scanner(System.in);
        System.out.println("Up to what number do you want to know the prime numbers? Answer:");
        x = scanner.nextInt();
        String primeNumbers = "";

        for ( i = 1; i <= x; i++) {
            int counter = 0;
            for (num = i; num>=1; num--) {
                if (i%num == 0) {
                    counter = counter + 1;
                }
            }
            if (counter == 2) {
                primeNumbers = primeNumbers + i + "; ";
            }
        }
        System.out.println("The prime numbers up to " + x + " are: ");
        System.out.println(primeNumbers);
    }
}
