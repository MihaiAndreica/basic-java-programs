package org.fasttrackit;

import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {
        String original, reverse = "";
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter the number or text to be checked if it is a palindrome:");
        original = in.nextLine();
        int length = original.length();
        for ( int i = length -1; i >= 0; i--)
            reverse = reverse + original.charAt(i);
        if (original.equals(reverse))
            System.out.println("Yes, your number/text is a palindrome!");
        else
            System.out.println("Your number/text is not a palindrome!");
    }
}
