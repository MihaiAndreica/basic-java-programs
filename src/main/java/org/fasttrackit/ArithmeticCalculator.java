package org.fasttrackit;

public class ArithmeticCalculator {
    public static void main(String[] args) {
        if (args.length == 3) {
            double a = Double.parseDouble(args[0]);
            double b = Double.parseDouble(args[2]);
            String operator = args[1];

            if (operator.equals("+")) {
                double result = a + b;
                System.out.println("Required equation: " + a + " - " + b + " = " + result);
            } else if (operator.equals("-")) {
                double result = a - b;
                System.out.println("Required equation: " + a + " - " + b + " = " + result);
            } else if (operator.equals("*")) {
                double result = a * b;
                System.out.println("Required equation: " + a + " - " + b + " = " + result);
            } else if (operator.equals("/")) {
                double result = a / b;
                System.out.println("Required equation: " + a + " - " + b + " = " + result);
            }
        } else {
            System.out.println("Please check the 3 CLI arguments required: first number, then operation symbol and second number!");
        }
    }
}
